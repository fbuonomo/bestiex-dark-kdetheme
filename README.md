<h2>BestieX-Dark-PlasmaTheme</h2>

<h3>Project Overview</h3>
I'm a Computer Science student and for fun I make this theme. 
I don't know how KDE Plasma works. This is my first work on this theme. 
I'm really grateful if someone can help me with. The main goal of this theme is to clone the macOS top bar in KDE. 

<h3>Install instruction</h3>

- Copy the dir in .local/share/plasma/desktoptheme

<h3>Screenshot</h3>
Desktop: <br>
<img src="Screenshot/Desktop.png">

Panel:<br>
<img src="Screenshot/Notification.png">

If you want the same config like the pictures just install la capitaine icon, otherwise the icons will be replaced with the icons of the system.

<h3>Credit</h3>
My special thanks go to Blacksaun19 for Network and Notification icons and keeferrourke for La Capitaine Icon.
